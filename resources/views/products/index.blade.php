@extends('template')

@section('content')

    @foreach($products as $product)

        <div class="col-md-4">
            <h2>{{ $product['title'] }}</h2>
            <p> {{ $product['price'] }} </p>
            <p> {{ $product['description'] }} </p>
            <p><a class="btn btn-primary" href="/products/{{ $product['alias'] }}" role="button">View details »</a></p>

            <p><a class="btn btn-success" href="/cart/{{ $product['alias'] }}" role="button">Buy »</a></p>
            @if(Auth::check())
                <p><a class="btn btn-success" href="/products/{{ $product['alias'] }}/edit" role="button">Edit »</a></p>
                <p>
                <form method="post" action="/products/{{ $product['alias']}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger">Delete</button>
                </form>
                </p>
            @endif

        </div>

    @endforeach

@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Welcome My Shop</h1>

        </div>
    </div>

@endsection