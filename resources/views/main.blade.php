@extends('template')

@section('content')

<div class="col-md-4">
    <h2>My pages:</h2>
    @if(Auth::check())
        <p><a class="btn btn-primary" href="/pages/create" role="button">Create »</a></p>
    @endif
    <p><a class="btn btn-success" href="/pages" role="button">Show »</a></p>

</div>

<div class="col-md-4">
    <h2>My products:</h2>
    @if(Auth::check())
        <p><a class="btn btn-primary" href="/products/create" role="button">Create »</a></p>
    @endif
    <p><a class="btn btn-success" href="/products" role="button">Show »</a></p>

</div>







@endsection

@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">MyShop.dev</h1>
            <p>Welcome</p>

        </div>
    </div>
@endsection