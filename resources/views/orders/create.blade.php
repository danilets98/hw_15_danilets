@extends('template')


@section('content')
<div class="col-md-12">
    <form action="/order" method="post" class="form-horizontal">
        @include('embed.errors')
        {{csrf_field()}}
        <div class="form-group">
       <h2>Selected products:</h2>
            <ul>
                @foreach($products as $product)
                    <li>
                        <strong>{{$product->title}}</strong>
                        x {{$cart[$product->id]}} : {{round($cart[$product->id] * $product->price,2)}} грн.
                    </li>
                @endforeach
            </ul>

        </div>

        <div class="form-group">
            <label for="customer_name">Full name:</label>
            <input type="text" name="customer_name" id="customer_name" class="form-control">
        </div>

        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" name="email" id="email" class="form-control">
        </div>

        <div class="form-group">
            <label for="phone">Phone:</label>
            <input type="text" name="phone" id="phone" class="form-control">
        </div>

        <div class="form-group">
            <label for="feedback">Feedback:</label>
            <textarea name="feedback" id="feedback"  class="form-control"></textarea>
        </div>

        <div class="form-group">
            <button class="btn btn-primary">Create order</button>
        </div>

    </form>
</div>


@endsection


@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Create order</h1>

        </div>
    </div>
@endsection