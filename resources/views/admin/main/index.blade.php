@extends('template')


@section('content')
<div class="col-md-12">
    @if($message = session('message'))
        <div class="alert alert-success" role="alert">
            {{$message}}
        </div>
    @endif

    <table class="table">
        <tr>
            <th>id</th>
            <th>User name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Products</th>
            <th></th>
        </tr>

        @foreach($orders as $order)
            <tr>
                <td>{{$order->id}}</td>
                <td>{{$order->customer_name}}</td>
                <td>{{$order->email}}</td>
                <td>{{$order->phone}}</td>
                <td>
                    <ul>
                        @foreach($order->products as $product)
                            <li>
                                {{$product->title}}
                                x {{$product->pivot->amount}}

                                <form action="/order/{{$order->id}}/{{$product->alias}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger">X</button>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    <form action="/order/{{$order->id}}" method="POST" >
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection



@section('jumbotron')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Admin panel</h1>

        </div>
    </div>
@endsection