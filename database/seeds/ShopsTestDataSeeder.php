<?php

use Illuminate\Database\Seeder;

class ShopsTestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('categories')->insert(
        [
            [   'type'=>'phones',
                'slug'=>'phones',
                'created_at'=>date("Y-m-d H:i:s")
            ],
            [   'type'=>'glasses',
                'slug'=>'glasses',
                'created_at'=>date("Y-m-d H:i:s")
            ],

        ]
    );

        DB::table('products')->insert(
        [
            [
                'title'=>'Apple iPhone X',
                'alias'=>'Apple-iPhone-X',
                'price'=>'38500',
                'description'=>'Екран (5.8", OLED (Super Retina HD), 2436x1125)
                 / Apple A11 Bionic / основна камера: подвійна 12 Мп, фронтальна камера: 
                 7 Мп / RAM 3 ГБ / 64 ГБ вбудованої пам\'яті / 3G / LTE / GPS / Nano-SIM / 
                 iOS 11',
                'category_id'=>1,
                'created_at'=>date("Y-m-d H:i:s")
            ],
            [
                'title'=>'Nokia 8 Dual Sim Tempered',
                'alias'=>'Nokia-8-Dual-Sim-Tempered',
                'price'=>'19000',
                'description'=>'Екран (5.3", IPS, 2560x1440) / Qualcomm Snapdragon 835
                 (4 x 2.45 ГГц + 4 x 1.9 ГГц) / основна камера: 13 Мп + 13 Мп,
                  фронтальна камера: 13 Мп / RAM 4 ГБ / 64 ГБ вбудованої пам\'яті + microSD 
                  / SDHC (до 256 ГБ) / 3G / LTE / GPS / підтримка 2 SIM-карток (Nano-SIM)
                   / Android 7.1 (Nougat) / 3090 мА·год',
                'category_id'=>1,
                'created_at'=>date("Y-m-d H:i:s")
            ],
            [
                'title'=>'Захисне скло Promate для Apple iPhone',
                'alias'=>'Promate-Protective-Glass-for-Apple-iPhone',
                'price'=>'300',
                'category_id'=>2,
                'description'=>'Сумісність: iPhone 6/6s
                    Вид: Протиударні
                    Розмір: 4.7 ',
                'created_at'=>date("Y-m-d H:i:s")
            ],
            [
                'title'=>'Захисне скло Promate для Nokia',
                'alias'=>'Promate-Protective-Glass-for-Nokia',
                'price'=>'320',
                'category_id'=>2,
                'description'=>'Сумісність: Nokia
                    Вид: Протиударні
                    Розмір: 4.7 ',
                'created_at'=>date("Y-m-d H:i:s")
            ]
        ]
        );

        DB::table('orders')->insert(
            [
                [
                    'customer_name'=>'John Snow',
                    'email'=>'johnSnow@gmail.com',
                    'phone'=>'0984523890',
                    'feedback'=>'Купил Apple iPhone X,всем доволен',
                    'created_at'=>date("Y-m-d H:i:s")
                ],
                [
                    'customer_name'=>'John Smith',
                    'email'=>'johnSmith@gmail.com',
                    'phone'=>'0986523890',
                    'feedback'=>'Купил Nokia 8 Dual Sim Tempered,прекрастное обслуживание',
                    'created_at'=>date("Y-m-d H:i:s")
                ],
                [
                    'customer_name'=>'Jack Johnson',
                    'email'=>'jackjohnson@gmail.com',
                    'phone'=>'0984574890',
                    'feedback'=>'Купил Захисне скло Promate для Apple iPhone,всем доволен',
                    'created_at'=>date("Y-m-d H:i:s")
                ],


            ]
        );


        DB::table('order_product')->insert(
            [
                [
                    'order_id'=>1,
                   'product_id'=>1,
                    'amount'=>1
                ],
                [
                    'order_id'=>2,
                   'product_id'=>1,
                    'amount'=>1
                ],
                [
                    'order_id'=>3,
                   'product_id'=>1,
                    'amount'=>1
                ]
            ]
        );

        DB::table('pages')->insert(
            [
                [
                    'title'=>'Страница 1',
                    'alias'=>'Page1',
                    'intro'=>'it\'s first page ',
                    'content'=>'it\'s first page ,it\'s first page ,it\'s first page ',
                    'created_at'=>date("Y-m-d H:i:s")
                ],
                [
                    'title'=>'Страница 2',
                    'alias'=>'Page2',
                    'intro'=>'it\'s second page ',
                    'content'=>'it\'s second page ,it\'s second page ,it\'s second page ',
                    'created_at'=>date("Y-m-d H:i:s")
                ],
                [
                    'title'=>'Страница 3',
                    'alias'=>'Page3',
                    'intro'=>'it\'s third page ',
                    'content'=>'it\'s third page ,it\'s third page ,it\'s third page ',
                    'created_at'=>date("Y-m-d H:i:s")
                ],


            ]
        );


    }
}
