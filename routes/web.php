<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','HomeController@home');



Route::resources([

    'products' =>'ProductsController',
    'pages' =>'PageController'
]);
//роуты для входа
Route::get('/login','Auth\LoginController@create')->name('login');
Route::post('/sessions', 'Auth\LoginController@store');
Route::get('/logout', 'Auth\LoginController@destroy');

// роуты для регистрации
Route::get('/register', 'Auth\RegisterController@create');
Route::post('/register', 'Auth\RegisterController@store');


Route::get('/cart/{product}', 'CartController@store');

Route::get('/order', 'OrderController@create');
Route::post('/order', 'OrderController@store');

Route::get('/category/{category}','CategoryController@index');

Route::delete('/order/{order}/{product}', 'Admin\OrdersController@destroyProduct');
Route::delete('/order/{order}', 'Admin\OrdersController@destroyOrder');

Route::get('/admin','Admin\MainController@index');