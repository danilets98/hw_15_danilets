<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{/// передаем данные только о товарах
///
  public function home(){
$products = Product::all();
$data['products'] = $products;
return view('main',$data);
}
}
